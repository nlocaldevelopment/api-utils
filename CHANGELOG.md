# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [X.X.X] - YYYY-MM-DD
### Added
- Somethind added.

### Changed
- Something changed.

### Removed
- Somethind removed.

## [0.2.0] - 2019-02-20
### Added
- Routing Error added.

### Changed
- Refactor around error handlers.

## [0.1.1] - 2019-02-19
### Changed
- README and CHANGELOG documentation files.

## [0.1.0] - 2019-02-19
### Added
- Api Errors handling.
- Utils Constraints: Version and Admin.

## [0.1.0] - 2019-02-19
### Added
- Api Errors handling.
- Utils Constraints: Version and Admin.

### Changed
- Files and Classes renamed to gem name.

## [0.0.0] - 2019-02-19
### Added
- Basic files to develop a basic gem.

[Unreleased]: https://bitbucket.org/nlocaldevelopment/api-utils/src/compare/0.1.0...HEAD
[0.1.0]: https://bitbucket.org/nlocaldevelopment/api-utils/src/releases/tag/0.1.0
[0.0.0]: https://bitbucket.org/nlocaldevelopment/api-utils/src/releases/tag/0.0.0