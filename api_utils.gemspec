
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'api_utils/version'

Gem::Specification.new do |spec|
  spec.name          = 'api_utils'
  spec.version       = ApiUtils::VERSION
  spec.authors       = ['Raúl Cabrera']
  spec.email         = ['raul.cabrera@publicar.com']

  spec.summary       = %q{Collection of utilities for APIs.}
  spec.description   = %q{Concerns ready for use in APIs.}
  spec.homepage      = 'https://bitbucket.org/nlocaldevelopment/api-utils/src/master/'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = spec.homepage
    spec.metadata['changelog_uri'] = 'https://bitbucket.org/nlocaldevelopment/api-utils/src/master/CHANGELOG.md'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '~> 2.6'

  ## development dependencies
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  # testing
  # spec.add_development_dependency 'rspec'
  # spec.add_development_dependency 'shoulda-matchers'
  # spec.add_development_dependency 'ffaker'
  # spec.add_development_dependency 'factory_bot'
  # debugger
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'awesome_print'
  # code analisys
  spec.add_development_dependency 'rubocop'
  # security
  # spec.add_development_dependency 'bundler-audit'
  # documentation
  spec.add_development_dependency 'yard'

  ## run time dependencies
  spec.add_runtime_dependency 'rails'

  ## requirements
  # spec.requirements << 'gem, v0.1'
end
