# frozen_string_literal: true

active_record = Object.const_get('ActiveRecord') rescue false

## dependencies
require 'active_record' if active_record
require 'action_controller'
require 'action_controller/metal'
require 'active_support'
require 'active_support/all'

# version
require 'api_utils/version'

# error handler
require 'api_utils/controllers/concerns/errors_handlers/standard'
require 'api_utils/controllers/concerns/errors_handlers/active_record' if active_record
require 'api_utils/controllers/concerns/errors_handlers/action_controller'

# constraints
require 'api_utils/constraints/version_constraint'
# require 'api_utils/constraints/admin_constraint'

module ApiUtils
  class Error < StandardError; end
  # Your code goes here...
end
