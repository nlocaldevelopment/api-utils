module ApiUtils
  class AdminConstraint
    def matches?(request)
      warden = request.env['warden']
      if warden
        warden.logout if warden.authenticated?
        warden.authenticate! scope: :api
        warden.user(:api).try(:roles).try(:any?) do |r|
          r.name=="super"
        end
      end
    end
  end
end