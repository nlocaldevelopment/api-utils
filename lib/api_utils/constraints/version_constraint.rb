module ApiUtils
  class VersionConstraint
    attr_reader :version, :vendor, :default

    def initialize(options)
      @version = options.fetch(:version)
      @vendor  = options.fetch(:vendor)
      @default = options[:default]
    end

    def matches?(request)
      (request.headers.try(:fetch, :accept).try(:include?,media_type) ||
      (@default  && request.headers.try(:fetch, :accept).try(:split,",").try(:map, &:lstrip ).try(:all?){|d| d.match(%r{^application\/vnd\.#{@vendor}\-v(\d+(\.|)){1,3}$}).blank?})
      )
    rescue => e
      false
    end

    private

    def media_type
      "application/vnd.#{@vendor}-v#{@version}"
    end
  end
end