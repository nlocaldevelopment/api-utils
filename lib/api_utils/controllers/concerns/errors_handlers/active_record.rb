module ApiUtils
  module ErrorsHandler
    module ActiveRecord
      extend ::ActiveSupport::Concern

      TYPES = [
              ::ActiveRecord::RecordNotFound,
              ::ActiveRecord::RecordInvalid
            ]

      included do
        TYPES.each do |error|
          handler = error.to_s.demodulize.underscore.to_sym
          rescue_from error, with: handler if self.respond_to?(:rescue_from,true)
        end
      end

      protected

      def record_not_found(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: {error: exception.message}, status: :not_found
      end

      def record_invalid(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: {error: exception.message}, status: :unprocessable_entity
      end
    end
  end
end