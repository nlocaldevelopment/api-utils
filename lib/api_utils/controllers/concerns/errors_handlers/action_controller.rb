module ApiUtils
  module ErrorsHandler
    module ActionController
      extend ::ActiveSupport::Concern

      TYPES = [
              ::ActionController::ParameterMissing,
              ::ActionController::RoutingError,
              ::ActionController::UnknownFormat
            ]

      included do
        TYPES.each do |error|
          handler = error.to_s.demodulize.underscore.to_sym
          rescue_from error, with: handler if self.respond_to?(:rescue_from,true)
        end
      end

      protected

      def parameter_missing(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: { error: exception.message }, status: :precondition_failed
      end

      def routing_error(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: { error: exception.message }, status: :not_found
      end

      def unknown_format(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: { error: exception.message }, status: :unsupported_media_type
      end
    end
  end
end