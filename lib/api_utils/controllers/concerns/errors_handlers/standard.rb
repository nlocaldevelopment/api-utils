module ApiUtils
  module ErrorsHandler
    module Standard
      extend ::ActiveSupport::Concern

      TYPES = [
              ::StandardError
            ]

      included do
        TYPES.each do |error|
          handler = error.to_s.demodulize.underscore.to_sym
          rescue_from error, with: handler if self.respond_to?(:rescue_from,true)
        end
      end

      protected

      def standard_error(exception)
        Rails.logger.error("Error : #{exception&.message} : " )
        Rails.logger.error("Backtrace: #{exception&.backtrace&.join("\n\t Backtrace: ")}")
        render json: { error: exception.message }, status: :internal_server_error
      end
    end
  end
end