#!/bin/bash

# iniciar el git
git init

# configuración git en local
git config --local color.ui true
git config --local core.editor "vim"
git config --local user.email "raul.cabrera@publicar.com"
git config --local user.name "Raúl Cabrera"

# configurar inicio git-flow proyect
git flow init -d

# release commit with react
git flow release start 0.0.0
git add .
git commit -m ':tada: created basic gem proyect'
git flow release finish '0.0.0' -m 'GEM PROYECT CREATED'

# asociar con repositorio remoto y primer push
# git remote add origin git@server.org:team/repo.git